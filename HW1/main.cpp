/* 
 * Name:  Ryan Fingerle
 * Student ID: 2419802
 * Date: 9/8/14 
 * HW: HW1
 * Problem: Display Sample 8/ Programming Project "Computer Science is Cool Stuff!"
 * I certify this is my own work and code 
 * Created on September 8, 2014, 6:47 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    
int number_of_pods , peas_per_pod , total_peas;

cout << "Press return after entering a number . \n";
cout << "Enter the number of pods : \n"; 

cin >> number_of_pods;

cout << "Enter the number of peas in a pod: \n";
cin >> peas_per_pod;

total_peas = number_of_pods * peas_per_pod;

cout << "If you have "; 
cout << number_of_pods;
cout << " peas pods \n";
cout << "and "; 
cout << peas_per_pod;
cout <<  "  peas in each pod, then \n";
cout << "you have "; 
cout << total_peas; 
cout << " peas in all the pods. \n";

cout << "This is the end of the program. \n";


cout << "*************************************************************************************\n";
cout << "\n";
cout << "                c c c                           s s s s                   !  !          \n";
cout << "            c           c                    s         s                  !  !          \n";
cout << "          c                                 s                             !  !           \n";
cout << "        c                                    s                            !  !           \n";
cout << "        c                                       s  s s s                  !  !           \n";
cout << "        c                                                s                !  !           \n";
cout << "          c                                              s                !  !           \n";
cout << "            c           c                   s          s                               \n";
cout << "               c c c                          s s s s                      00          \n";
cout << "\n";
cout << "***************************************************************************************\n";
cout << "      Computer Science is Cool Stuff! ! ! !\n";

    return 0;
}

